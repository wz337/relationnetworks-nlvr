import json
import os
import pickle
import re
import copy
import numpy as np

import torch
# from tqdm import tqdm
# import config


# def compute_class(answer):
#     for key, values in classes.items():
#         if answer in values:
#             return key
#
#     raise ValueError('Answer {} does not belong to a known class'.format(answer))

def build_dictionaries(nlvr_dir):
    sentence_dirs = os.path.join(nlvr_dir, 'sentences')
    if not os.path.exists(sentence_dirs):
        os.makedirs(sentence_dirs)

    cached_dictionaries = os.path.join(nlvr_dir, 'sentences', 'NLVR_build_dictionaries.pkl')
    if os.path.exists(cached_dictionaries):
        print('==> using cached dictionaries:{}'.format(cached_dictionaries))
        print(cached_dictionaries)
        with open (cached_dictionaries, 'rb') as f:
            return pickle.load(f)

    sentence_to_idx = {}
    answer_to_idx = {'false': 0, 'true': 1}
    json_train_filename = os.path.join(nlvr_dir, 'train', 'train.json')
    json_dev_filename = os.path.join(nlvr_dir, 'dev', 'dev.json')

    # load json file into a list
    json_data = []
    with open(json_train_filename, "r") as f:
        lines = f.readlines()
        for line in lines:
            line = line.strip()
            json_data.append(json.loads(line))

    # with open(json_dev_filename, "r") as f:
    #     lines = f.readlines()
    #     for line in lines:
    #         line = line.strip()
    #         json_data.append(json.loads(line))

    for jd in json_data:
        sentence = tokenize(jd['sentence'])
        for word in sentence:
            if word not in sentence_to_idx:
                #one-based indexing #zero is reserved for padding
                sentence_to_idx[word] = len(sentence_to_idx) + 1

    #adding an unknown token for the word that did not appear in the training set
    sentence_to_idx['unknown'] = len(sentence_to_idx) + 1

    res = (sentence_to_idx, answer_to_idx)
    with open(cached_dictionaries, 'wb') as f:
        pickle.dump(res, f)

    return res


def to_dictionary_indexes(dictionary, sentence):
    """
    Outputs indexes of dictionary corresponding to the words in the sequence. (case-insensitive)
    :param dictionary: which dictionary
    :param sentence:
    :return:
    """
    split = tokenize(sentence)
    # idxs = torch.LongTensor([dictionary[w] for w in split])

    sentence_idx = []
    for w in split:
        if w in dictionary:
            sentence_idx.append(dictionary[w])
        else:
            sentence_idx.append(dictionary['unknown'])

    idxs = torch.LongTensor(torch.from_numpy(np.array(sentence_idx)))

    return idxs

def collate_samples_image(batch):
    return collate_samples(batch, False)

def collate_samples_state_description(batch):
    return collate_samples(batch, True)

def collate_samples(batch, state_description):
    """
    Used by DatasetLoader to merge together multiple samples into one mini-batch
    :param batch:
    :param state_description:
    :return:
    """
    images = [d['image'] for d in batch]
    sentences = [d['sentence'] for d in batch]
    answers = [d['answer'] for d in batch]

    #questions are padded to a fixed length (the maximum length in questions)
    #so they can be inserted in a tensor
    batch_size = len(batch)
    max_len = max(map(len, sentences))

    padded_sentences = torch.LongTensor(batch_size, max_len).zero_()
    for i, q in enumerate(sentences):
        padded_sentences[i, :len(q)] = q

    collate_batch = dict(
        image=torch.stack(images),
        answer=torch.stack(answers),
        sentence=torch.stack(padded_sentences)
    )

    return collate_batch


def tokenize(sentence):
    s = re.sub('([.,;:!?()])', r' \1 ', sentence)
    s = re.sub('\s{2,}', ' ', s)

    return [w.lower() for w in s.split()]


def load_tensor_data(data_batch, cuda, invert_sentences, volatile=False):
    # prepare input
    var_kwargs = dict(volatile=True) if volatile else dict(requires_grad=False)

    sentence = data_batch['sentence']
    if invert_sentences:
        # invert question indexes in this batch
        sentence_len = sentence.size()[1]
        sentence = sentence.index_select(1, torch.arange(sentence_len - 1, -1, -1).long())

    img = torch.autograd.Variable(data_batch['image'], **var_kwargs)
    sentence = torch.autograd.Variable(sentence, **var_kwargs)
    label = torch.autograd.Variable(data_batch['answer'], **var_kwargs)
    if cuda:
        img, sentence, label = img.cuda(), sentence.cuda(), label.cuda()

    # label = (label-1).squeeze(1)
    label = (label).squeeze(1)
    return img, sentence, label

def json_transformer(nlvr_dir, split):
    """
    The original JSON file from the official dataset has numbers of questions equal to the number of total questions
    divided by 6. Every entry in the original JSON file corresponds to 6 different permutations of image. In order to
    pair individual image and question, the function takes in the original json file and creates a new json file.

    :param nlvr_dir: Root directory of NLVR dataset
    :param train: boolean parameter indicating whether we are transforming train or val. Train=True, Val=False
    :return: Filename of the new JSON file
    """
    sentence_dirs = os.path.join(nlvr_dir, 'sentences')
    if not os.path.exists(sentence_dirs):
        os.makedirs(sentence_dirs)

    sentence_json_filename = os.path.join(nlvr_dir, 'sentences', split + '_6x.json')
    # if train:
    #     split = 'train'
    #     sentence_json_filename = os.path.join(nlvr_dir, 'sentences', split + '_6x.json')
    # else:
    #     split = 'dev'
    #     sentence_json_filename = os.path.join(nlvr_dir, 'sentences', split + '_6x.json')

    if not os.path.exists(sentence_json_filename):
        original_json_filename = os.path.join(nlvr_dir, split, split + '.json')

        json_data = []
        with open(original_json_filename, "r") as f:
            lines = f.readlines()
            for line in lines:
                line = line.strip()
                json_data.append(json.loads(line))

        new_json_data = []
        num_of_permute = 6

        for data in json_data:
            for i in range(num_of_permute):
                new_data = copy.deepcopy(data)
                new_data['permutation'] = str(i)
                new_json_data.append(new_data)

        with open(sentence_json_filename, 'w') as f:
            for data in new_json_data:
                line = json.dumps(data)
                f.write(line)
                f.write('\n')

    return sentence_json_filename

# dicts = build_dictionaries('/Users/iriszhang/CornellTech/cs5304/relation-network/RelationNetworks-NLVR/nlvr')
# print(dicts[0]['unknown'])